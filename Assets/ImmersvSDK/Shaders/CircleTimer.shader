﻿Shader "Immersv/CircleTimer" {
	Properties{
		_minRadius("Minimum Radius", Range(0.0, 0.5)) = 0.35
		_maxRadius("Maximum Radius", Range(0.0, 0.5)) = 0.4
		_fill("Fill percentage", Range(0.0, 1.0)) = 0.4	
		_edge("Edge length", Range(0.0, 1.0)) = 0.1
		
		_baseColor ("Base Color", Color) = (1.0,1.0,1.0,1.0)
		_finalColor ("Final Color", Color) = (0.5,0.5,1.0,1.0)
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 150
				Pass{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag

		#include "UnityCG.cginc"

		sampler2D _MainTex;
		sampler2D _MovieTex;
		float _baseIlum;
		
		float _minRadius;
		float _maxRadius;
		float _fill;
		float _edge;
		
		float4 _baseColor;
		float4 _finalColor;

		struct v2f {
			float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;
		};

		float4 _MainTex_ST;

		v2f vert(appdata_base v)
		{
			v2f o;
			o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
			o.uv = v.texcoord;

			return o;
		}


		float4 frag(v2f i) : SV_Target
		{
			float2 toPoint = i.uv - float2(0.5,0.5);
			float r = length(toPoint);
			
			if(r < _minRadius || r > _maxRadius)
			{
				discard;
			}
			
			float pi = 3.1415926;
			float pi2 = pi*2;
			float theata = atan2(toPoint.y,toPoint.x) + pi;

			float fill = theata / pi2;
			
			if(fill > _fill)
			{
				discard;
			}
							
			float colorFactor = saturate(10 * (fill + _edge - _fill));
				
			return lerp( _baseColor , _finalColor, colorFactor);
		}

		ENDCG
		}
		}
	Fallback "Mobile/VertexLit"
}