﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Ball : MonoBehaviour {

	public float ballInitialVelocity;
    private Text countText;


    private Rigidbody rb;
	private bool ballInPlay;
    private int count;

	void Awake () {

		rb = GetComponent<Rigidbody>();
        countText = GameObject.Find("Count Text").GetComponent<Text>();

    }

    void Start ()
    {
        count = 0;
        SetCountText();
    }

    public float deploymentHeight;
    private bool deployed;

    void Update () 
	{
		if (Cardboard.SDK.Triggered && ballInPlay == false)
		{
			transform.parent = null;
			ballInPlay = true;
			rb.isKinematic = false;
			rb.AddForce(new Vector3(ballInitialVelocity, ballInitialVelocity, 0));
		}

        RaycastHit hit;
        Ray landingRay = new Ray(transform.position, Vector3.down);

        Debug.DrawRay(transform.position, Vector3.down * deploymentHeight);

        if (!deployed)
        {
            if (Physics.Raycast(landingRay, out hit, deploymentHeight))
            {
                if (hit.collider.tag == "Cube")
                {
                    Teleport.instance.TeleportCube();
                    count = count + 1;
                    SetCountText();
                }
            }
        }

    }

    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString();
    }

}