﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {

	public float paddleSpeed;


	private Vector3 playerPos = new Vector3 (0, -11.3f, 0);

	void Update () 
	{
//		float xPos = transform.position.x + (Input.GetAxis("Horizontal") * paddleSpeed);
		float xPos = transform.position.x + (Cardboard.SDK.HeadPose.Position.x * paddleSpeed);
        float yPos = transform.position.y + (Cardboard.SDK.HeadPose.Position.y * paddleSpeed);
        playerPos = new Vector3 (Mathf.Clamp(xPos, -7f, 9f), Mathf.Clamp(yPos, -11.3f, -3f), 0f);
		transform.position = playerPos;

	}
}