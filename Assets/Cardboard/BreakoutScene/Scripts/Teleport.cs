﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {

    public static Teleport instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
    
    public void TeleportCube() {
        
        transform.localPosition = new Vector3(Random.Range(-7, 9), Random.Range(0, 5), 0);

    }
}
