﻿using System;
using UnityEngine;
using Immersv;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GM : MonoBehaviour {

    private const string APPLICATION_ID = "0938-3832-0098";
    private const string PLACEMENT_ID = "9767-8344-0099";
    public bool _isSDKReady = false;

    public int lives = 3;
    //	public Text livesText;
    //	public GameObject gameOver;
    public GameObject paddle;
    public GameObject cube;
	public static GM instance = null;

	private GameObject clonePaddle;

	void Awake () 
	{

        PrepareSDK();

        if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);

		Setup();
        
        DontDestroyOnLoad(this);
        
    }

	public void Setup()
	{
		clonePaddle = Instantiate(paddle, transform.position, Quaternion.identity) as GameObject;
        Instantiate(cube, new Vector3(UnityEngine.Random.Range(-7, 9), UnityEngine.Random.Range(0, 5), 0), Quaternion.identity);
    }

    void PrepareSDK()
    {
        ImmersvSDK.OnInitSuccess += () =>
        {
            _isSDKReady = true;
            ImmersvSDK.Ads.OnAdReady += () =>
            {
                ImmersvSDK.Ads.StartAd();
            };

            ImmersvSDK.Ads.OnAdError += (string message) => 
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            };

            ImmersvSDK.Ads.OnAdComplete += (AdViewResult result) =>
            {
                if (result.BillableCriteriaMet)
                {
                    //RewardUser();
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
            };

        };
        ImmersvSDK.Init(APPLICATION_ID);
    }

    void ShowAd()
    {
        ImmersvSDK.Ads.LoadAd(PLACEMENT_ID);
        
    }

    public void LoseLife()
	{
        lives--;

        //		livesText.text = "Lives: " + lives;
        Debug.Log(lives);

        Destroy(clonePaddle);
        Destroy(GameObject.FindWithTag("Ball"));

        if (lives < 1 && _isSDKReady)
        {
            lives = 3;
            ShowAd();
        }

        else {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        

    }

}